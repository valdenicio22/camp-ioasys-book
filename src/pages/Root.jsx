import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import { AuthProvider } from '../components/Context/AuthContext'
import { RoutesPrivate } from '../components/Routes/Private/Private'

import { PagesHome } from './Home/Home';
import { PagesLogin } from './Login/Login';

import { GlobalStyle } from '../styles/global'

export const PagesRoot = () => (
  <Router>
    <AuthProvider>
      <Switch>
        <Route path="/login" component={PagesLogin} />
        <RoutesPrivate path="/" component={PagesHome} />
      </Switch>
    </AuthProvider>
    <GlobalStyle />
  </Router>
)