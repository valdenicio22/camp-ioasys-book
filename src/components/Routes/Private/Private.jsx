import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { Context } from '../../Context/AuthContext'

export const RoutesPrivate = ({ component: Component, ...rest }) => {

  const { authenticated, loading } = React.useContext(Context);
  if(loading) {
    return null
  }
  return (
    <Route
      {...rest}
      render={() => authenticated
        ? <Component {...rest} />
        : <Redirect to="/login" />
      }
    />
  )
}