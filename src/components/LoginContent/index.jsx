import React from 'react';
import { Context } from '../Context/AuthContext';

import { ContainerBackIMG, ContainerForm, ContainerHeader } from "./style";
import logo from '../../assets/Logo.svg';

function initialState() {
  return { email: '', password: '' };
}

export const LoginContent = () => {

  const ctx = React.useContext(Context);

  const [values, setValues] = React.useState(initialState);
  const [error, setError] = React.useState(null);

  function handleOnChange(event) {
    const { value, name } = event.target;
    setValues({
      ...values,
      [name]: value,
    });
  }

  function handleSubmit(event) {
    event.preventDefault();

    ctx.handleLogin(values).catch(_ => {
      setError(
         'Email ou senha inválido'
      )
    })
  }

  return (
    <>
      <ContainerBackIMG>
        <ContainerForm onSubmit={handleSubmit}>
          <ContainerHeader>
            <img src={logo} alt="logo ioasys" />
            <p>Books</p>
          </ContainerHeader>
          <label htmlFor="email">
            Email
          <input id="user" type="text" name='email' onChange={handleOnChange} value={values.email} />
          </label>
          <label htmlFor="password" className='password'>
            Senha
            <input id="password" type="password" name='password' onChange={handleOnChange} value={values.password} />
            {error && (<div style={{color: 'red'}}>{error}</div>)}
            <button>Entrar</button>
          </label>
        </ContainerForm>
      </ContainerBackIMG>

    </>
  );
}