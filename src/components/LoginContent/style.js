import styled from 'styled-components'
import backgroundIMG from '../../assets/BackgroundImage.svg';

const media = {
  desktop: '@media(max-width: 1080px)'
}

export const ContainerBackIMG = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: url(${backgroundIMG});

`
export const ContainerForm = styled.form`

  width: 368px;
  height: 224px;
  display: flex;
  flex-direction: column;

  position: absolute;
  top: 30%;
  left: 110px;

  input{
    width: 100%;
    height: 60px;
    background: rgba(0, 0, 0, 0.32);
  }
  .password{
    position: relative;

    button{
      position: absolute;
      top:20%;
      right:0;
      bottom: 0;
    
      cursor: pointer;
      border-radius: 50%;
      padding: 0 1rem;
    }
  }
`

export const ContainerHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: left;
  margin-bottom: 2rem;

  img{
      margin-right: 1rem;
  }
`