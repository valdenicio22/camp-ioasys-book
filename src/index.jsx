import React from 'react';
import { render } from 'react-dom';
import { PagesRoot } from './pages/Root'

render(<PagesRoot />, document.getElementById('root'));